<?php


namespace Rockschtar\Exceptions\Generic;


class NotANumberExceptions extends InvalidArgumentException {
    public function __construct(string $parameter, $value, ?string $message) {
        $message = $message ?? sprintf('Value "%s" for parameter "%s" is not a number', $value, $parameter);
        parent::__construct($parameter, $value, $message);
    }
}