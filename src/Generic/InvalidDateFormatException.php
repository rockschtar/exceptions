<?php


namespace Rockschtar\Exceptions\Generic;


class InvalidDateFormatException extends InvalidArgumentException {
    public function __construct($argument, $value, string $expected_format = 'm-d-Y', ?string $message) {
        $message = $message ?? sprintf('Date string "%s" has an unexpected format. Expected "%s" is not a number.', $value, $expected_format);
        parent::__construct($argument, $value, $message);
    }
}