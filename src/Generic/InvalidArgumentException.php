<?php


namespace Rockschtar\Exceptions\Generic;


class InvalidArgumentException extends \Exception {

    public function __construct(string $argument, $value, ?string $message = null) {
        $value = \is_object($value) ? \get_class($value) : $value;
        $message = $message ?? sprintf('Invalid value %s" for argument "%s"', $value, $argument);
        parent::__construct($message);
    }
}